    <div class="form-group">
        {!! Form::label('title', 'Název:') !!}
        {!! Form::text('title', null, ['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('url', 'URL:') !!}
        {!! Form::text('url', null, ['class'=>'form-control', 'placeholder'=>'www.neco.com']) !!}
    </div>
    <div class="form-group">
        {!! Form::submit($submitButtonText, ['class'=>'btn btn-primary form-control']) !!}
    </div>