@extends('layouts.app')

@section('content')
    <h1>Úprava feedu</h1>
    {!! Form::model($feed, ['method'=>'PATCH', 'url'=>'feedy/'.$feed->id]) !!}
    @include('feeds._form',['submitButtonText'=>'Update'])
    {!! Form::close() !!}
    @include('errors.list')
@endsection