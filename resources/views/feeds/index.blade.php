@extends('layouts.app')
@section('content')
    <h1>Přehled feedů</h1>
    <table class="table table-striped">

    @foreach($feeds as $key => $feed)
    <tr>

            <td>{{$feed->title}}</td>
            <td>{{$feed->url}}</td>
            <td>{{$feed->created_at}}</td>
            <td>{{$feed->updated_at}}</td>
            <td><a href="{{url('feedy/'.$feed->id.'/edit')}}">Edit</a></td>

    </tr>
    @endforeach
    </table>
@endsection