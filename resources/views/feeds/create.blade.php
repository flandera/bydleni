@extends('layouts.app')

@section('content')
    <h1>Vložení nového feedu</h1>
    {!! Form::model($feed = new App\Feed, ['url'=>'feedy']) !!}
        @include('feeds._form',['submitButtonText'=>'Přidat Feed'])
    {!! Form::close() !!}
    @include('errors.list')
@endsection