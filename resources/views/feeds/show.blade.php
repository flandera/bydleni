@extends('layouts.app')
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">{{$feed->title}}</div>
        <div class="panel-body">
            <span class="row">{{$feed->url}}</span>
            <span class="row">{{$feed->created_at}}</span>
        </div>

    </div>

@endsection