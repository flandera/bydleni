@if(Session::has('flash_message'))
        <div class="alert {{session::has('flash_message_important') ? 'alert-danger alert-important' : 'alert-success'}}">
            @if(session::has('flash_message_important'))
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            @endif
            {{Session::get('flash_message')}}
        </div>

@endif