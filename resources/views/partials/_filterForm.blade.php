<div class="row">
    <div class="frm form-inline">
    {{ Form::model($tags, ['method'=>'show', 'url'=>'tag/'.$tag->id]) }}
        <div class="form-group">
            {!! Form::label('Tag', 'Filter:') !!}
            {!! Form::select('tag_list[]',$tags, $article->TagList, ['id'=>'tag_list','class'=>'form-control']) !!}
            {!! Form::submit('Filtruj',['class'=>'btn btn default']) !!}
        </div>
    </div>
</div>