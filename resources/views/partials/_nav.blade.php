<nav class="navbar navbar-default">
    <div class="navbar-header">

        <!-- Collapsed Hamburger -->
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
            <span class="sr-only">Toggle Navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

        <!-- Branding Image -->
        <a class="navbar-brand" href="{{ url('/') }}">
            Centrum bydlení
        </a>
    </div>

    <div class="collapse navbar-collapse" id="app-navbar-collapse">
        <!-- Left Side Of Navbar -->
        {{--<ul class="nav navbar-nav">--}}
            {{--<li><a href="{{ url('/home') }}">Home</a></li>--}}
        {{--</ul>--}}
        {{--<ul class="nav navbar-nav">--}}
            {{--<ul class="nav navbar-nav">--}}
                {{--<li class="dropdown">--}}
                    {{--<a href="/clanky" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">--}}
                        {{--Články <span class="caret"></span>--}}
                    {{--</a>--}}
                    {{--<ul class="dropdown-menu" role="menu">--}}
                        {{--<li><a href="{{ url('/clanky') }}">Články</a></li>--}}
                        {{--<li><a href="{{ url('/clanky/moje') }}">Moje</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
            {{--</ul>--}}
        {{--</ul>--}}
        <ul class="nav navbar-nav">
            <li><a href="{{ url('/clanky') }}">Články</a></li>
        </ul>
        @if(!Auth::guest())
            <ul class="nav navbar-nav">
                <li><a href="{{ url('/clanky/moje') }}">Moje<img src = "/images/library32.png" title="knihovna"></a></li>
            </ul>
        @endif
        @if(!Auth::guest()&&Auth::user()->hasRole('admin'))
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="/feedy" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        Feedy <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url('/feedy') }}">Přehled</a></li>
                        <li><a href="{{ url('/feedy/create') }}">Nový</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="/statistiky" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        Statistiky <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url('/statistiky') }}">Články</a></li>
                        <li><a href="{{ url('/statistiky/tags') }}">Tagy</a></li>
                    </ul>
                </li>
            </ul>
            @endif
        <!-- Right Side Of Navbar -->
        <ul class="nav navbar-nav navbar-right">
            <!-- Authentication Links -->
            @if (Auth::guest())
                <li><a href="{{ url('/login') }}">Přihlásit</a></li>
                <li><a href="{{ url('/register') }}">Registrovat</a></li>
            @else
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Odhlásit</a></li>
                    </ul>
                </li>
            @endif
        </ul>
    </div>
</nav>