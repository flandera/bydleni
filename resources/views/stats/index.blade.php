@extends('layouts.app')
@section('content')
    <div class="panel panel-heading">
        <h2>{{ $header }}</h2>
    </div>
    <div class="panel panel-body">
        <div class="frm form-inline">
            @if(!($type =='tag'))
                {{ Form::open(array('route' => 'statistiky.index', 'method' => 'get')) }}
            @else
                {{ Form::open(array('action' => 'StatsController@tags', 'method' => 'get')) }}
            @endif

            <div class="form-group col-md-11">
                <div class="input-group col-md-12">
                    <div class="input-group-addon">
                        Od:
                    </div>

                        {!! Form::date('dateFrom', $dateFrom->format('d-m-Y'), ['class'=>'form-control datepicker']) !!}
                    <div class="input-group-addon">
                        Do:
                    </div>
                         {!! Form::date('dateTo',$dateTo->format('d-m-Y'), ['class'=>'form-control datepicker']) !!}

                </div>
            </div>
            {!! Form::submit('Filtruj',['class'=>'btn btn-primary']) !!}
            {{Form::close()}}

        </div>
        <table class="table table-striped" id="stats">
            <thead>
                <tr>
                    @foreach($tableheader as $field)
                    <th>{{$field}}</th>
                    @endforeach
                </tr>
            </thead>
            <tbody>

            @foreach($stats as $stat)

                <tr>
                    @foreach($stat as $key=>$item)

                        @if($key=='url')
                                <td> <a href="{{$item}}">{{substr($item,0,30)}}</a></td>
                        @else
                            @if($key=='id'&&$type=='article')
                                <td><a href="statistiky/{{$stat['id'].'?dateFrom='.$dateFrom->format('d-m-Y').'&dateTo='.$dateTo->format('d-m-Y')}}">{{$item}}</a></td>
                            @elseif(($key=='id'&&$type=='tag'))
                                <td><a href="/statistiky/tag/{{$stat['id'].'?dateFrom='.$dateFrom->format('d-m-Y').'&dateTo='.$dateTo->format('d-m-Y')}}">{{$item}}</a></td>
                            @else
                                <td>{{$item}}</td>
                            @endif

                        @endif
                    @endforeach
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    {!! Form::open(array('url' => array('statistiky/download'),'method' => 'get')) !!}
    {!! Form::hidden('type',$type) !!}
    {!! Form::hidden('dateFrom',$dateFrom->format('d-m-Y')) !!}
    {!! Form::hidden('dateTo',$dateTo->format('d-m-Y')) !!}
    {!! Form::submit('Export dat',['class'=>'btn btn-primary']) !!}
    {!! Form::close() !!}
@endsection
@section('footer')
    <script>
        $(function() {
            $.each($(".datepicker"), function(){
             $(this).datepicker({
                 firstDay: 1,
                 dayNames: [ "Neděle", "Pondělí", "Úterý", "Středa", "Čtvrtek", "Pátek", "Sobota" ],
                 dayNamesMin: [ "Ne", "Po", "Út", "St", "Čt", "Pá", "So" ],
                 dayNamesShort: [ "Ned", "Pon", "Útr", "Stř", "Čtv", "Pát", "Sob" ],
                 monthNames: [ "Leden", "Únor", "Březen", "Duben", "Květen", "Červen", "Červenec", "Srpen", "Září", "Říjen", "Listopad", "Prosinec" ],
                 dateFormat: "dd-mm-yy"
             });
            });
        });
    </script>
@endsection