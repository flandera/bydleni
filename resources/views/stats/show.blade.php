@extends('layouts.app')
@section('content')
    <div class="panel panel-heading">
        <h2>{{ $header }}</h2>
    </div>
    @if(count($stats)==0)
        Data dosud nebyly vypočteny, opakujte prosím později
    @else
    <p>Legenda: Graf ukazuje denní změny údajů, kromě počtu v mých článcích, která jsou souhrnná k danému datu. Počet článků je zobrazován pouze pro tagy.</p>
    <p> Průměrné hodnocení článku je aktuální hodnota v daný den.</p>
    <div class="panel panel-body">
        <canvas id="canvas"></canvas>
    </div>

        @if($type == 'article')
            {!! Form::open(array('url' => array('statistiky/download/'.$article->id), 'method' => 'get', 'type'=>'article')) !!}
        @else
            {!! Form::open(array('url' => array('statistiky/download/'.$tag->id),'method' => 'get')) !!}
        @endif
        {!! Form::hidden('type',$type) !!}
        {!! Form::hidden('dateFrom',$dateFrom->format('d-m-Y')) !!}
        {!! Form::hidden('dateTo',$dateTo->format('d-m-Y')) !!}
        {!! Form::submit('Export dat',['class'=>'btn btn-primary']) !!}
        {!! Form::close() !!}
    @endif

@endsection
@section('footer')
    @include('footer')
    <script>
        var randomScalingFactor = function() {
            return Math.round(Math.random() * 100 * (Math.random() > 0.5 ? -1 : 1));
        };
        var randomColorFactor = function() {
            return Math.round(Math.random() * 255);
        };
        var randomColor = function(opacity) {
            return 'rgba(' + randomColorFactor() + ',' + randomColorFactor() + ',' + randomColorFactor() + ',' + (opacity || '.3') + ')';
        };

        if (type=='article'){
            var title = "Statistiky článku";
        }else{
            var title = "Statistiky tagu";
        }
       var config = {
            type: 'line',
            data: {
                labels: dates,
                datasets: [{
                    label: "Moje články",
                    data: my_count,
                    fill: false,
                    borderDash: [5, 5],
                },
                    {
                    label: "Počet kliknutí",
                    data: click_count,
                    fill: false,
                    borderDash: [5, 5],
                }, {
                    label: "Počet hodnocení",
                    data: rating_count,
                    lineTension: 0,
                    fill: false,
                }, {
                    label: "Průměrné hodnocení",
                    data: avg_rating,
                    fill: false,
                },{
                    label: "Počet článků",
                    data: articles_count,
                    fill: false,
                }

                ]
            },
            options: {
                responsive: true,
                legend: {
                    position: 'bottom',
                },
                hover: {
                    mode: 'label'
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Den'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Hodnota'
                        },
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                },
                title: {
                    display: true,
                    text: title
                }
            }
        };
        $.each(config.data.datasets, function(i, dataset) {
            var background = randomColor(0.5);
            dataset.borderColor = background;
            dataset.backgroundColor = background;
            dataset.pointBorderColor = background;
            dataset.pointBackgroundColor = background;
            dataset.pointBorderWidth = 1;
        });

       window.onload = function() {
            var ctx = document.getElementById("canvas").getContext("2d");
            window.myLine = new Chart(ctx, config);
        };
    </script>
@endsection