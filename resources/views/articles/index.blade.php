@extends('layouts.app')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')
<div class="panel panel-heading">
    <h2>{{ $header }}</h2>
</div>
    <div class="panel panel-body">
        <div class="frm form-inline ">
        {{ Form::open(array('url' => 'clanky/hledat', 'method' => 'get')) }}
        {{Form::hidden('inputpage',$inputpage)}}
        <div class="form-group col-md-11">
            <div class="input-group col-md-10">
                <div class="input-group-addon">
                    Text:
                </div>
                    {!! Form::text('keywords', null, ['class'=>'form-control']) !!}
            </div>
        </div>
        {!! Form::submit('Filtruj',['class'=>'btn btn-primary']) !!}
        {{Form::close()}}
    </div>
        <div class="frm form-inline ">
            {{ Form::open(array('route' => 'tag.index', 'method' => 'get')) }}
            {{Form::hidden('inputpage',$inputpage)}}
            <div class="form-group col-md-11">
                <div class="input-group col-md-12">
                    <div class="input-group-addon">
                    Tag:
                    </div>
                    {!! Form::select('tag',$tags, null, ['class'=>'form-control', 'id'=>'tag_filter']) !!}
                </div>
            </div>
            {!! Form::submit('Filtruj',['class'=>'btn btn-primary']) !!}
            {{Form::close()}}
        </div>

    </div>
    @foreach($articles as $key => $article)

        <div class="panel panel-body" article="{{$article['id']}}">

            <div class="media" article="{{$article['id']}}">
                @if(!$article['picture']==null)
                <div class="media-left col-lg-3">
                    <a href="{{$article['url']}}">
                        <img class="media-object" src="{{$article['picture']}}" alt="...">
                    </a>
                </div>
                @endif
                <div class="media-body">
                    <a href="{{$article['url']}}">
                        <h4 class="media-heading">{{$article['title']}}</h4>
                    </a>
                    <p>{{$article['published_at']}}</p>
                    {{substr($article['body'],0,250).'...'}}

                </div>
            </div>
           <div class="row">
                    <div class="frm form-inline">
                        {{ Form::model($article, ['method'=>'PATCH', 'url'=>'clanky/'.$article->id]) }}
                            <div class="form-group col-lg-3">
                                <div class="rating" id="{{$article->avg_rating.'_'.$article->id}}" article="{{$article->id}}"></div>
                            </div>
                            <div class="form-group col-lg-12">
                                <div class="input-group col-lg-9">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-tags"></span>
                                    </div>
                                    @if($role=='admin')
                                        {!! Form::select('tag_list[]',$tags, $article->TagList, ['id'=>'tag_list','class'=>'form-control', 'multiple']) !!}
                                        {!! Form::submit('OK', ['class'=>'btn btn-primary']) !!}
                                    @else
                                        {!! Form::select('tag_list[]',$tags, $article->TagList, ['id'=>'tag_list','class'=>'form-control', 'multiple', 'disabled']) !!}
                                    @endif
                                    {!! Form::hidden('page', app('request')->input('page')) !!}
                               </div>
                                @if(!$myArticles->contains($article))
                                    {!! Form::button('<span class="glyphicon glyphicon-plus"></span>'."<img src = '/images/library18.png' title='Přidat do knihovny'>", ['class'=>'btn btn-info','id'=>'addMy']) !!}
                                    {!! Form::button('<span class="glyphicon glyphicon-remove"></span>'."<img src = '/images/library18.png' title='Odebrat z knihovny'>", ['class'=>'btn btn-warning','id'=>'removeMy', 'disabled'=>'disabled', 'style'=>'visibility:hidden']) !!}
                                @else
                                    {!! Form::button('<span class="glyphicon glyphicon-plus"></span>'."<img src = '/images/library18.png' title='Přidat do knihovny'>", ['class'=>'btn btn-info','id'=>'addMy', 'disabled'=>'disabled', 'style'=>'visibility:hidden']) !!}
                                    {!! Form::button('<span class="glyphicon glyphicon-remove"></span>'."<img src = '/images/library18.png' title='Odebrat z knihovny'>", ['class'=>'btn btn-warning','id'=>'removeMy']) !!}
                                @endif
                                {{--{!! Form::button('Smazat', ['class'=>'btn btn-danger pull-right','id'=>'remove']) !!}--}}
                                @if($role!='guest')
                                    {!! Form::button('Smazat', ['class'=>'btn btn-danger pull-right','data-toggle'=>'modal','data-target'=>'.modal-remove','id'=>'modal-remove']) !!}
                                @else
                                    {!! Form::button('Smazat', ['class'=>'btn btn-danger pull-right','data-target'=>'.modal-remove','id'=>'modal-remove']) !!}
                                @endif
                            </div>

                        {{ Form::close() }}
                    </div>
           </div>
       </div>


@endforeach
        <!-- Small modal remove article-->
       <div class="modal fade modal-remove" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
            <div class="modal-dialog modal-sm">
               <div class="modal-content">
                   <div class="modal-header">
                       <h4 class="modal-title">Varování!</h4>
                   </div>
                   <div class="modal-body modal-remove">
                       Chcete skutečně smazat tento článek?
                   </div>

                   <div class="modal-footer">
                       <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                       <button type="button" class="btn btn-danger" id="remove">Smazat</button>
                   </div>
                </div>

            </div>
        </div>

<div class="panel-footer">
   {!! $articles->links() !!}
</div>

@endsection

@section('footer')
    <script>
        $('div.alert').not('.alert-important').delay(3000).slideUp(300);
        var page = $('ul li.active span').text();
        {{session('page', \Illuminate\Pagination\Paginator::resolveCurrentPage())}}
        //console.log(page);
        var role = '{{  ($role) }}';
        var inputpage = '{{  ($inputpage) }}';
        console.log(inputpage);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        //console.log($('#tag_list').find('option').attr('value'));
        $('#tag_filter').select2({
            placeholder: 'vyberte tag',
            tags: true,

        });

        $.each($('.frm'), function(){
            var id = $(".rating", this).attr('article');

            //select2 u clanku pro tagy
            $('#tag_list', this).select2({
                placeholder: 'vyberte tag',
                tags: true,

            });

            //proklik pres tagy pod clankem na filtr tagu
            if(!(role=='admin')){
                var options = $('#tag_list', this).children();
                $.each($('.select2-selection__choice'), function(){
                    var name = $(this).text().substring(1);
                   $(this).hover(function(evt){
                       evt.preventDefault();
                       //$(this).parent().disabled=false;
                       $(this).css( 'cursor', 'pointer' );
                       $(this).css( 'background-color', '#337ab7');
                       $(this).css( 'color', 'white');
                  },function(evt){
                       $(this).css( 'background-color', '#e4e4e4');
                       $(this).css( 'color', 'black');
                   });
                    $(this).click(function(evt) {
                        evt.preventDefault();

                        var link = '/tag/'+name;
                        console.log(name);
                        window.location.href = link;
                    });
                });
            }


            //hodnoceni clanku
            $('.rating', this).jRating({
                //id: this.class,
                bigStarsPath : '/build/css/icons/stars.png', // path of the icon stars.png
                smallStarsPath : '/build/css/icons/small.png',
                phpPath: "/clanky/"+id+"/rate",
                step:true, // no half-star when mousemove
                length: 7, // nb of stars
                decimalLength:0, // number of decimal in the rate
                rateMax: 7,
                admin: (role=='admin'||role=='user')
            });

//              puvodni implementace primo na tlacitko smazat
//            $('#remove',this).click(function(evt) {
//                evt.preventDefault();
//                if(!(role=='user'||role=='admin')){
//                    var link = 'clanky/unauthorized';
//                    window.location.href = link;
//                }else{
//                    $.ajax({
//                        type: 'POST',
//                        url: '/clanky/' + id + '/rate',
//                        data: {
//                            value: 0,
//                        },
//                        complete: function (data) {
//                            console.log(id);
//                            if(role=='user'||role=='admin'){
//                                $(".panel[article='"+id+"']").remove();
//                            }
//                        }
//                    });
//                }
//            });

            //nova implemetace odstraneni clanku s popup oknem
            $('#modal-remove',this).click(function(evt) {
                evt.preventDefault();
                if(!(role=='user'||role=='admin')){
                    var link = 'clanky/unauthorized';
                    window.location.href = link;
                }else{
                    $('#remove').attr('article',id);
                   }
            });

            //pridani do oblibenych
            $('#addMy',this).click(function(evt) {
                evt.preventDefault();
                if(!(role=='user'||role=='admin')){
                    var link = 'clanky/unauthorized';
                    window.location.href = link;
                }else{
                    $.ajax({
                        type: 'POST',
                        url: '/clanky/' + id + '/addMyArticle',
                    });
                    $(this).prop('disabled',true);
                    $(this).siblings('#removeMy').prop('disabled',false);
                    $(this).css('visibility','hidden');
                    $(this).siblings('#removeMy').css('visibility','visible');
                }
            });
            //odebrani z oblibenych
            $('#removeMy',this).click(function(evt) {
                evt.preventDefault();
                if(!(role=='user'||role=='admin')){
                    var link = 'clanky/unauthorized';
                    window.location.href = link;
                }else{
                    $.ajax({
                        type: 'POST',
                        url: '/clanky/' + id + '/removeMyArticle',
                        complete: function (data) {
                            console.log(id);
                            if((role=='user'||role=='admin')&&inputpage=='my'){
                                $(".panel[article='"+id+"']").remove();
                            }
                        }
                    });
                    $(this).prop('disabled',true);
                    $(this).siblings('#addMy').prop('disabled',false);
                    $(this).css('visibility','hidden');
                    $(this).siblings('#addMy').css('visibility','visible');
                }
            });

        });

        //proklik clanku s odchycenim
        $.each($('.media'), function(){
            var id = $(this).attr('article');
            $('a', this).click(function(evt) {
                evt.preventDefault();
                var link = $(this).attr('href');
                $.ajax({
                    type: 'POST',
                    url: '/clanky/'+id+'/addclick',
                    data: {
                        href: link,

                    },
                    complete: function (data) {
//                        console.log(link);
//                        window.location.href = link;
                        var win=window.open(link, '_blank');
                        win.focus();
                    }
                });
            });
            // console.log(data);
        });
        //nova implemetace odstraneni clanku s popup oknem
        $('#remove').click(function(evt){
            var id = $(this).attr('article');
            //console.log(id);
            $.ajax({
                type: 'POST',
                url: '/clanky/' + id + '/rate',
                data: {
                    value: 0,
                },
                complete: function (data) {
                    //console.log(id);
                    if(role=='user'||role=='admin'){
                        $(".panel[article='"+id+"']").remove();
                    }
                    $('.modal-remove').modal('hide');
                }
            });
        })

    </script>
@endsection