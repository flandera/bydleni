<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Click extends Model
{
    protected $fillable = [
        'article_id',
        'user_id',
        'ip'
    ];

    public function article(){
        return $this->belongsTo('App\Article');
        //->withTimestamps();
    }
}
