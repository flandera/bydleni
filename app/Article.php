<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Article extends Model
{
    protected $fillable = [
        'title',
        'url',
        'published_at',
        'body',
        'picture'
    ];

    protected $dates = ['published_at'];

    //protected $attributes = ['user_id'];

    /**
     * @param $query omezeni na nesmazane clanky
     */
    public function scopeNonDeleted($query){
        $query->whereNotIn('id',$this->nullRating());
    }

    /**získava clanky prirazene do mych
     * @param $query
     */
    public function scopeMyArticles($query){
        $query->whereIn('id',$this->myArticles());
    }

    /**scope fo articles with at least one tag
     * @param $query
     */
    public function scopeTagged($query){
//        /dd($this->tagged());
        $query->whereIn('id',$this->tagged());
    }

    /**
     * konvence setNameAttribute
     * @param $data
     */
    public function setPublishedAtAttribute($data){
      $this->attributes['published_at'] = Carbon::createFromFormat('d F Y, g:i a', $data);
    }

    public function getPublishedAtAttribute($data)
    {
        return Carbon::parse($data)->format('d. m. Y H:i');
    }

//    public function setUserIdAttribute(){
//        $this->attributes['user_id'] = Auth::user()->id;
//    }
//    public function getUserIdAttribute(){
//        return Auth::user()->id;
//    }

//    public function users(){
//        return $this->belongsToMany('App\User', 'article_tag', 'article_id','user_id')->withPivot('tag_id');
//    }
    /**
     * get tags associated with the given article
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags(){
        return $this->belongsToMany('App\Tag')->withTimestamps();
        //->withPivot('user_id')->withTimestamps();
    }

    /**
     * get rating associated with the given article by current user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function ratings(){
        return $this->hasMany('App\Rating');
    }

    /**
     * @return mixed list id clanku s hodnoceni null(=smazane)
     */
    public function nullRating(){
      return Rating::adminRating()->where('value',null)->lists('article_id');
    }

    /**
     * @return mixed rating of articles of current user
     */
    public function myArticles(){
        $ratings = Rating::where('user_id',Auth::user()->id)->lists('article_id');
        //$tagged =;
        return Rating::where('user_id',Auth::user()->id)->lists('article_id');
    }

    public function tagged(){
        $tags = Tag::all();
        foreach($tags as $tag){
            if(count($tag->articles()->get())!=0){
                $articles=Article::whereIn('id',$tag->articles()->lists('article_id'))->lists('id');
                foreach($articles as $article){
                    //dd($article);
                    $art[]=$article;
                }
            }
        }
//        /dd($art);
        return $art;
    }

        /**
     * return list of tags for article
     * @return array
     */
    public function getTagListAttribute(){
        return $this->tags->lists('id')->toArray();
    }

    /**
     * return cliks for article
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clicks(){
        return $this->hasMany('App\Click');
    }

    public function feed(){
        return $this->belongsTo('App\Feed');
    }

    public function users(){
        return $this->belongsToMany('App\User');
    }

    public function articleDailyStats(){
        return $this->hasMany('App\ArticleDailyStat');
    }
    public function articleWeeklyStats(){
        return $this->hasMany('App\ArticleWeeklyStat');
    }
    public function articleMonthlyStats(){
        return $this->hasMany('App\ArticleMonthlyStat');
    }
}
