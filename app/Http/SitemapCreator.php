<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 16.5.2016
 * Time: 14:53
 */

namespace App\Http;

use App\Article;
use App\Tag;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\URL;
use Roumen\Sitemap\Sitemap;
use Illuminate\Contracts\Container\Container;

class SitemapCreator
{
    private $sitemap;

    public function __construct()
    {
        $this->siteMap = $this->sitemap();
    }

    public function sitemap(){
        //$sitemap =new Sitemap(['/',Carbon::now()]);
        //$app = App::make('Listener');
        $sitemap = App::make("sitemap");
        //dd($sitemap);
        $sitemap->add(URL::to('/'), Carbon::now(), '1.0', 'daily');
        //$sitemap->setCache('laravel.sitemap', 60);
//        if (!$sitemap->isCached())
//        {
            // add items to the sitemap (url, date, priority, freq)
//        $sitemap->add(URL::to(), '2012-08-25T20:10:00+02:00', '1.0', 'daily');
//        $sitemap->add(URL::to('page'), '2012-08-26T12:30:00+02:00', '0.9', 'monthly');

            // get all posts from db
            $tags = Tag::all();

            // add every post to the sitemap
            foreach ($tags as $tag)
            {
                //dd($article->published_at);
//                $sitemap->add($article->url,Carbon::createFromFormat('d. m. Y H:i',$article->published_at)->toDateTimeString(), 1, 'daily',[0=>['url'=>$article->picture, 'title' => $article->title,'caption' => $article->title]], $article->title);
                $sitemap->add("/tag/".$tag->name,Carbon::now(), 1, 'daily');
            }
            $sitemap->add(URL::to('/clanky'), Carbon::now(), '1.0', 'daily');
            // generate your sitemap (format, filename)
            $sitemap->store('xml','sitemap');
//        }

    }
}