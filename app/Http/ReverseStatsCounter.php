<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 11.5.2016
 * Time: 11:22
 */

namespace App\Http;

use App\ArticleDailyStat;
use App\Article;
use Carbon\Carbon;
use App\Tag;
use App\TagDailyStat;
class ReverseStatsCounter
{
    public function __construct()
    {
        $compute = $this->reverseCompute();
        //$compute = $this->reverseComputeTagsDailyStats();

    }

    public function reverseCompute(){
        $dailystats = ArticleDailyStat::all();
        $articles = Article::all();
        $date = Carbon::parse('2016-04-01');
        for($date; $date < Carbon::now()->endOfDay()->subDay(1);$date->addDay() ){
            foreach ($articles as $article) {
                $upperBorder = clone $date;
                $upperBorder->addDay(1);
                $hits = $article->clicks()->whereBetween('created_at', [$date->startOfDay(), $upperBorder])->get();
                $my = $article->users()->whereBetween('article_user.created_at', [$date->startOfDay(),$upperBorder])->get();
                $rates = $article->ratings()->notNull()->whereBetween('created_at',[$date->startOfDay(),$upperBorder])->get();
                //dd(['datum'=>$date,'horni mez'=>$upperBorder,'clanek'=>$article,'kliky'=>count($hits),'my'=>count($my),'ratings'=>count($rates)]);
                if(!(count($hits)==0&&count($my)==0&&count($rates)==0)){
                    ArticleDailyStat::updateOrCreate([
                        'article_id'=>$article->id,
                        'date'=>$date->toDateString()],
                        ['article_id'=>$article->id,
                            'date'=>$date->toDateString(),
                            'clicks_count'=>count($hits),
                            'rating_count'=>count($rates),
                            'avg_rating'=>$article->avg_rating,
                            'my_count'=>count($my)]);
                }
            }
        }


        return;
    }
}