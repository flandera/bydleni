<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Tag;
use Laracasts\Utilities\JavaScript\JavaScriptServiceProvider;
use App\Role;
use Laracasts\Flash\FlashServiceProvider;
use Laracasts\Flash;
use App\Click;
use App\Rating;
use App\User;
use App\Feed;
use App\Article;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\ArticleRequest;
use JavaScript;


class TagsController extends Controller
{


    public function index(){
        $id = request()->tag;
        $inputpage = 'index';
        if(request()->inputpage){
            $inputpage = request()->inputpage;
        }

        //dd($page);
        $tag = Tag::where('id',$id)->firstOrFail();


        $role = $this->role;
        $allArticles = $articles = Article::latest('published_at')->nonDeleted()->get(['id']);
        //dd($allArticles);
        $articles = $tag->articles()->whereIn('article_id',$allArticles)->latest()->paginate();
        $tags = Tag::lists('name','id');
        if(Auth::guest()){
            $role = 'guest';
            $myArticles = collect([]);
        }else{
            $role = Auth::user()->roles()->firstOrFail()->name;
            $myArticles = Auth::user()->articles()->get();
            //dd($myArticles);
        }
        //dd($inputpage);
        if($inputpage=='index'){
            $header = 'Články s tagem '.$tag->name;
            return view('articles.index', compact('articles','tags','header','role','myArticles','inputpage'));
        }else{
            return redirect(action('ArticlesController@my', ['tag'=>$tag]));
        }

    }

    public function show(Tag $tag){
        $role = $this->role;
        $inputpage = 'index';
        if(request()->inputpage){
            $inputpage = request()->inputpage;
        }
        $header = 'Články s tagem '.$tag->name;
        $allArticles = $articles = Article::latest('published_at')->nonDeleted()->get(['id']);
        $articles = $tag->articles()->whereIn('article_id',$allArticles)->latest()->paginate();
        //$articles = $tag->articles()->get();
        $tags = Tag::lists('name','id');
        if(Auth::guest()){
            $role = 'guest';
            $myArticles = collect([]);
        }else{
            $role = Auth::user()->roles()->firstOrFail()->name;
            $myArticles = Auth::user()->articles()->get();
            //dd($myArticles);
        }
        //$articles = Article::latest('published_at')->nonDeleted()->get();
        return view('articles.index', compact('articles','tags','header','role','myArticles','inputpage'));
    }

    public function filter(Request $request){

    }
}
