<?php

namespace App\Http\Controllers;

use App\Click;
use App\Http\ArticleDailyStatsCounter;
use App\Rating;
use App\Tag;
use App\User;
use Illuminate\Http\Request;
use App\Feed;
use App\Article;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Session\Store;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Laracasts\Utilities\JavaScript\JavaScriptServiceProvider;
use willvincent\Feeds\Facades\FeedsFacade;
use willvincent\Feeds\FeedsFactory;
use Feeds;
use Illuminate\Support\Facades\Auth;
use App\Http\ArticleCrawler;
use App\Http\Requests\ArticleRequest;
use JavaScript;
use App\Role;
use Laracasts\Flash\FlashServiceProvider;
use Laracasts\Flash;
use Illuminate\Database\Eloquent;
use Illuminate\Database;
use App\Http\SitemapCreator;
use Sphinx;



class ArticlesController extends Controller
{
    private $crawler = null;
    private $dailystat = null;
    private $siteMap = null;
    /**
     * @var Store
     */
    private $store;


    public function __construct(){
        //TODO kontorla stahovani na produkci
        //$this->crawler = new ArticleCrawler();
        //$this->dailystat = new ArticleDailyStatsCounter();
        //$this->siteMap = new SitemapCreator;
        $this->middleware('auth',['except'=>['index','addClick','unauthorized']]);
    }

    public function index(){
        $header = 'Přehled všech článků';
        $inputpage = 'index';
        $articles = Article::latest('published_at')->nonDeleted()->paginate();
        if(Auth::guest()){
            $role = 'guest';
            $myArticles = collect([]);

//            /dd($articles);
        }else{
            $role = Auth::user()->roles()->firstOrFail()->name;
            $myArticles = Auth::user()->articles()->get();
            //dd($myArticles);
        }

        if($role!='admin'){
            $articles = Article::latest('published_at')->nonDeleted()->tagged()->paginate();
        }

        $tags = Tag::lists('name','id');

        return view('articles.index', compact('articles','tags','role','header','myArticles', 'inputpage'));
    }

    public function edit(Article $article){
        $allratings = $article->ratings()->notNull()->get();
        //dd($allratings);
        return view('articles.edit',compact('article'));
    }

    /** view pro moje články
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function my(){
        $header = 'Moje články';
        $inputpage = 'my';
        $tags = Tag::lists('name','id');
        if(!Auth::guest()){
            $user = Auth::user();
            $role = $user->roles()->firstOrFail()->name;
            $myArticles = $articles = $user->articles()->orderBy('published_at','DESC')->get();
            $articles = $user->articles()->orderBy('published_at','DESC')->paginate();
            //dd(request()->articles);
            if(request()->tag){
                $id = request()->tag;
                $tag = Tag::where('id',$id)->firstOrFail();
                $myArticlesIds = $articles = $user->articles()->orderBy('published_at','DESC')->lists('article_id');
                //dd($myArticlesIds);
                $articles = $tag->articles()->whereIn('article_id',$myArticlesIds)->paginate();
                //dd($articles);
                $header = 'Moje články s tagem '.$tag->name;
                $inputpage = 'my';
            }
            return view('articles.index', compact('articles','tags','role','header','myArticles', 'inputpage'));
        }else{
            return redirect('articles.index',401);
        }
    }
    /**
     * @param Article $article
     * @param ArticleRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function rate(Article $article, ArticleRequest $request){
        if ($request->isMethod('post')){
                $rating = $this->createRating($article, $request['rate']);
            if($request['rate']==0){
                $user = Auth::user();
                $user->articles()->detach($article);
                return response()->json(['success' => '1']);
            }
                $updateArticle = $this->updateAverageRating($article);
                return response()->json(['success' => '1']);


            }
        }

    public function update(Article $article, ArticleRequest $request){
        //dd($request->all());
        $page = $request->input('page');
        $article->update($request->All());
        if(!(is_null($request->input('tag_list')))){
          $this->syncTags($article, $request->input('tag_list'));
        }else{
            $article->tags()->detach();
        }
        flash()->success('Tagy byly uloženy');
        if($page!=1&&$page!=''){
            return Redirect::action('ArticlesController@index',['page'=>$page]);
        }
        return redirect('clanky');
    }

    /**
     * @param $article
     * @param $value hodnota ratingu
     * @return objekt ratingu
     */
    public function createRating($article, $value){
        $rating = Rating::updateOrCreate([
            'article_id'=>$article->id,
            'user_id'=>Auth::user()->id
        ],[
            'article_id'=>$article->id,
            'user_id'=>Auth::user()->id,
            'value'=>$value
        ]);
        return $rating;
    }
    /**
     * sync up the list of tags
     * @param Article $article
     * @param tags
     */
    private function syncTags(Article $article, $tags)
    {
        $user = Auth::user();
        //dd($user);
        $currentTags = array_filter($tags, 'is_numeric');
        $newTags = array_diff($tags, $currentTags);

        foreach($newTags as $key =>$newTag){
            $tabKey = Tag::create(['name'=>$newTag]);
            $tags[$key]= $tabKey->id;
        }
//        foreach($tags as $key=>$tag){
//            $user->articles()->updateOrCreate(['tag_id'=>$tag, 'article_id'=>$article->id]);
//        }
//        foreach($tags as $key=>$tag){
//
//            $data[]=(['tag_id'=>$tag, 'user_id'=>$user->id, 'article_id'=>$article->id]);
//        }

        $article->tags()->sync((array)$tags);
        //$user->tags()->sync(['tag_id'=>(array)$tags, 'article_id'=>$article->id]);
    }

    /**
     * @param $article
     * @return mixed
     */
    private function updateAverageRating($article){
        $total = 0;
        $allratings = $article->ratings()->notNull()->get();
        foreach($allratings as $allrate){
            $total += $allrate->value;
        }
        $averageRating = $total/count($allratings);
        $article->avg_rating = $averageRating;
        $updated = $article->save();
        return $updated;
    }

    public function addClick(Article $article, ArticleRequest $request){
        if ($request->isMethod('post')){
            $ip = $_SERVER['REMOTE_ADDR'];
            //dd($request);
            $addclick = $this->createClick($article, $ip);
            //TODO dodělat vrácení notifikace
            return response()->json([$addclick]);
        }
    }

    public function createClick($article, $ip){
        if(!Auth::guest()){
            $userId = Auth::user()->id;
        }else{
            $userId = null;
        }
        $click = $article->clicks()->create(['user_id'=>$userId,
            'ip'=>$ip]);
        return $click;
    }

    public function addMyArticle(Article $article, ArticleRequest $request){
        if ($request->isMethod('post')) {
            $user = Auth::user();
            $myArticle = $user->articles()->attach($article);
            return response()->json([$myArticle]);
        }

    }

    public function removeMyArticle(Article $article, ArticleRequest $request){
        if ($request->isMethod('post')){
            $user = Auth::user();
            $myArticle = $user->articles()->detach($article);
            return response()->json([$myArticle]);
        }

    }

    public function unauthorized(Request $request){
        flash()->overlay('Přihlásit', 'Pro tuto funkci musíte být přihlášen');
        //session()->flash('flash_message_important', true);
        return redirect('clanky');
    }

    public function search(){
        $words = request(['keywords']);
        $sphinx = new Sphinx;
        //$words = 'byt';
        $results = $sphinx->search($words,'test1')->get();
        $justArticles = collect($results['matches']);
        $ids = implode(",",array_keys($results['matches']));
        //dd($ids);
//        $opts = array();
//        $coloured = $sphinx->excerpts($spinxArticles[0], $opts);
//        dd($coloured);
        //echo ($coloured);
        $header = 'Přehled všech článků s klíčovým slovem: '.$words;
        $inputpage = 'index';
        //$articles = Article::latest('published_at')->nonDeleted()->paginate();
        $articles = Article::whereIn('id',$justArticles->keys())->nonDeleted()->orderByRaw("FIELD(id, ".$ids.")")->paginate();
        if(Auth::guest()){
            $role = 'guest';
            $myArticles = collect([]);
        }else{
            $role = Auth::user()->roles()->firstOrFail()->name;
            $myArticles = Auth::user()->articles()->get();
        }

        if($role!='admin'){
            $articles = Article::latest('published_at')->nonDeleted()->tagged()->paginate();
        }

        $tags = Tag::lists('name','id');

        return view('articles.index', compact('articles','tags','role','header','myArticles', 'inputpage'));

    }
}
