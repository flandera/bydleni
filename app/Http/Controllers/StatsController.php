<?php

namespace App\Http\Controllers;

use App\Article;
use App\ArticleDailyStat;
use App\Http\ArticleDailyStatsCounter;
use App\Http\ReverseStatsCounter;
use App\Http\ReverseTagStatsCounter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Click;
use App\Tag;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Laracasts\Utilities\JavaScript\JavaScriptFacade;
use Maatwebsite\Excel\Facades\Excel;


class StatsController extends Controller
{
    public $dateFrom;
    public $dateTo;

    public function __construct()
    {
        if(request()->dateFrom&&request()->dateTo){
            $this->dateFrom = Carbon::createFromFormat('d-m-Y', request()->dateFrom);
            $this->dateTo = Carbon::createFromFormat('d-m-Y', request()->dateTo);
        }else{
            $this->dateFrom = Carbon::now()->subDay(30);
            $this->dateTo = Carbon::now();
        }
$stats = null;
    }

    /**vytažení statistik pro články
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        $dateFrom = $this->dateFrom;
        $dateTo = $this->dateTo;
        $type = 'article';
        //dd($dateFrom);
        $header = 'Statistiky článků ';
        $tableheader = ['Id', 'Článek','URL','Počet kliků', 'Počet hodnocení','Průměrné hodnocení','V mých článcích'];
        $stats = collect($this->getStats());
        $stats = $stats->sortByDesc('clicks_count');
        return view('stats.index', compact('header','stats','tableheader','dateFrom','dateTo','type'));
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tags(){
        $dateFrom = $this->dateFrom;
        $dateTo = $this->dateTo;
        $type = 'tag';
        $header = 'Statistiky tagů';
        $tableheader = ['Id', 'Tag', 'url', 'Počet kliků','Počet hodnocení','V mých článcích','Průměrné hodnocení','Počet článků'];
        $stats = collect($this->getTagsStats());
        $stats = $stats->sortByDesc('click_count');
        //dd($stats);
        return view ('stats.index', compact('header','stats','tableheader','dateFrom','dateTo','type'));
    }


    public function getStats(){
        if(request()->dateFrom&&request()->dateTo){
            $this->dateFrom = Carbon::createFromFormat('d-m-Y', request()->dateFrom);
            $this->dateTo = Carbon::createFromFormat('d-m-Y', request()->dateTo);
        }
        $articles = Article::all();
        $stats = [];
        foreach ($articles as $article) {
           if($article->avg_rating!=null){
               $hits = $article->clicks()->whereBetween('created_at', [$this->dateFrom,$this->dateTo])->get();
               $my = $article->users()->whereBetween('article_user.created_at', [$this->dateFrom,$this->dateTo])->get();
               $rates = $article->ratings()->whereBetween('created_at',[$this->dateFrom,$this->dateTo])->get();
               if(!(count($hits)==0&&count($my)==0&&count($rates)==0)) {

                   $stats[$article->id]=['id'=>$article->id,
                       'title'=>$article->title,
                       'url'=>$article->url,
                       'clicks_count'=>count($hits),
                       'rating_count'=>count($rates),
                       'avg_rating'=>$article->avg_rating,
                       'my_count'=>count($my)];
               }
           }

        }

        return $stats;
    }

    public function getTagsStats(){
        if(request()->dateFrom&&request()->dateTo){
            $this->dateFrom = Carbon::createFromFormat('d-m-Y', request()->dateFrom);
            $this->dateTo = Carbon::createFromFormat('d-m-Y', request()->dateTo);
        }

        $tags = Tag::all();
        $stats = [];
        foreach ($tags as $tag){
            //cliky započítat až od připojení článku k tagu
            $articles = $tag->articles()->get();
            if(count($articles)!=0){
                foreach ($articles as $article) {
                    $dayOfAttachment = $tag->articles()->where('article_tag.article_id',$article->id)->FirstOrFail()->created_at;
                    //dd($dayOfAttachment);
                    if($dayOfAttachment>=$this->dateFrom){
                        $hits = $article->clicks()->whereBetween('created_at', [$dayOfAttachment,$this->dateTo])->get();
                        //echo ('<p>'.count($hits).'</p>');
                        $my = $article->users()->whereBetween('article_user.created_at', [$dayOfAttachment,$this->dateTo])->get();
                        $rates = $article->ratings()->whereBetween('created_at',[$dayOfAttachment,$this->dateTo])->get();
                    }else{
                        $hits = $article->clicks()->whereBetween('created_at', [$this->dateFrom,$this->dateTo])->get();
                        $my = $article->users()->whereBetween('article_user.created_at', [$this->dateFrom,$this->dateTo])->get();
                        $rates = $article->ratings()->whereBetween('created_at',[$this->dateFrom,$this->dateTo])->get();
                    }

                    if(!(count($hits)==0&&count($my)==0&&count($rates)==0)) {
                        if(isset($stats[$tag->id])){
                            $stats[$tag->id]['click_count']+=count($hits);
                            $stats[$tag->id]['rating_count']+=count($rates);
                            $stats[$tag->id]['my_count']+=count($my);
                            $stats[$tag->id]['articles_count']++;
                            $stats[$tag->id]['avg_rating']+=$article->avg_rating;
                        }else{
                            $stats[$tag->id]['id']=$tag->id;
                            $stats[$tag->id]['name']=$tag->name;
                            $stats[$tag->id]['url']='/tag/'.$tag->name;
                            $stats[$tag->id]['click_count']=count($hits);
                            $stats[$tag->id]['rating_count']=count($rates);
                            $stats[$tag->id]['my_count']=count($my);
                            $stats[$tag->id]['avg_rating']=$article->avg_rating;
                            $stats[$tag->id]['articles_count']=1;
                        }

                    }
                }
                if(isset($stats[$tag->id])&&count($stats[$tag->id])!=0){

                    $stats[$tag->id]['avg_rating'] = $stats[$tag->id]['avg_rating']/ $stats[$tag->id]['articles_count'];
                    //dd($stats[$tag->id]);
                }
            }


            //dd($stats);
        }

        return $stats;
    }

    public function show(Article $article){
    $header = 'Statistiky článku '.$article->id.' - '.$article->title;
    $stats = $article->articleDailyStats()->orderBy('date')->get();
    $type = 'article';
    $data = [];
    $dateTo = $this->dateTo;
        if(count($stats)!=0){
            $avgLastValue= 0;
            foreach($stats as $stat){
                $dateFrom = clone($this->dateFrom);


                for($dateFrom; $dateFrom->startOfDay()<=$dateTo->endOfDay(); $dateFrom->addDay()){
                    //echo('<p>'.$dateFrom.'</p>'.'<p>'.$avgLastValue.'</p>');
                    if($stat->date->endOfDay()==$dateFrom->endOfDay()){
                        //dd(['statdate'=>$stat->date->endOfDay(),'datefrom'=>$dateFrom->endOfDay()]);
                        $data['dates'][$dateFrom->toDateString()]=$stat->date->toDateString();
                        $data['my_count'][$dateFrom->toDateString()]=$stat->my_count;
                        $data['rating_count'][$dateFrom->toDateString()]=$stat->rating_count;
                        $data['clicks_count'][$dateFrom->toDateString()]=$stat->clicks_count;
                        $data['avg_rating'][$dateFrom->toDateString()]=$stat->avg_rating;
                        $avgLastValue = $stat->avg_rating;
//dd($avgLastValue);
                    }else{
                        if(!isset($data['dates'][$dateFrom->toDateString()])) {
                            $data['dates'][$dateFrom->toDateString()] = $dateFrom->toDateString();
                            $data['my_count'][$dateFrom->toDateString()] = 0;
                            $data['rating_count'][$dateFrom->toDateString()] = 0;
                            $data['clicks_count'][$dateFrom->toDateString()] = 0;
                            $data['avg_rating'][$dateFrom->toDateString()] = $avgLastValue;
                            //dd(['datum'=>$dateFrom,'avg'=>$avgLastValue]);
                       }

                    }

                    //dd($avgLastValue);
                    //echo("<p>".$data['avg_rating'][$dateFrom->toDateString()]."</p>");
                }

            }
            //dd($data);
            \JavaScript::put([
                'type' => 'article',
                'dates' => array_values($data['dates']),
                'my_count' => array_values($data['my_count']),
                'click_count'=>array_values($data['clicks_count']),
                'rating_count'=>array_values($data['rating_count']),
                'avg_rating'=>array_values($data['avg_rating']),
                'articles_count'=>[],
            ]);
        }
    $dateFrom = $this->dateFrom;
    return view ('stats.show', compact('stats','header', 'data','article','type','dateFrom','dateTo'));
    }

    public function showTag($tag){
        $tag = Tag::where('id',$tag)->FirstOrFail();
        $data = [];
        $header = 'Statistiky tagu '.$tag->id.' - '.$tag->name;
        $type = 'tag';
        $stats = $tag->tagDailyStats()->orderBy('date')->get();

        $dateTo = $this->dateTo;
        if(count($stats)!=0){
            $avgLastValue= 0;
            $articlesCount = 0;
            foreach($stats as $stat){
                $dateFrom = clone($this->dateFrom);

                for($dateFrom; $dateFrom->startOfDay()<=$dateTo->endOfDay(); $dateFrom->addDay()){
                    if($stat->date->endOfDay()==$dateFrom->endOfDay()){
                        $data['dates'][$dateFrom->toDateString()]=$stat->date->toDateString();
                        $data['my_count'][$dateFrom->toDateString()]=$stat->my_count;
                        $data['articles_count'][$dateFrom->toDateString()]=$stat->articles_count;
                        $data['rating_count'][$dateFrom->toDateString()]=$stat->rating_count;
                        $data['clicks_count'][$dateFrom->toDateString()]=$stat->clicks_count;
                        $data['avg_rating'][$dateFrom->toDateString()]=$stat->avg_rating;
                        $avgLastValue= $stat->avg_rating;
                        $articlesCount = $stat->articles_count;
                    }else{
                        if(!isset($data['dates'][$dateFrom->toDateString()])) {
                            $data['dates'][$dateFrom->toDateString()] = $dateFrom->toDateString();
                            $data['my_count'][$dateFrom->toDateString()] = 0;
                            $data['rating_count'][$dateFrom->toDateString()] = 0;
                            $data['clicks_count'][$dateFrom->toDateString()] = 0;
                            $data['articles_count'][$dateFrom->toDateString()] = $articlesCount;
                            $data['avg_rating'][$dateFrom->toDateString()] = $avgLastValue;

                        }

                    }

                }
                //echo("<p>".$data['avg_rating'][$dateFrom->toDateString()]."</p>");
                //
            }
            //dd($data);
            //dd($stats);
            \JavaScript::put([
                'type' =>'tag',
                'dates' => array_values($data['dates']),
                'articles_count'=>array_values($data['articles_count']),
                'my_count' => array_values($data['my_count']),
                'click_count'=>array_values($data['clicks_count']),
                'rating_count'=>array_values($data['rating_count']),
                'avg_rating'=>array_values($data['avg_rating'])
            ]);
        }

        $dateFrom = $this->dateFrom;
        return view ('stats.show', compact('stats','header', 'data', 'type','tag','dateFrom','dateTo'));
    }

    /**zpetny vypocet statistik za predchozi dny
     * @return string
     */
    public function compute(){
        $reverseStat = new ReverseStatsCounter();
        //$re->reverseCompute();
        return 'Stats reComputed';
    }
    public function computeTags(){
        $reverseTagStat = new ReverseTagStatsCounter();
        return 'Tag Stats reComputed';
    }

    public function getExcel($id){
        //dd(request());
        $type = request(['type']);
        $dateFrom = $this->dateFrom;
        $dateTo = $this->dateTo;
        //dd($dateFrom);
        if($type=='article'){
            $id = Article::where('id',$id)->firstOrFail();
            $name = 'Statistiky_clanku_'.$id->id;
            $data = $id->articleDailyStats()->whereBetween('date', [$dateFrom->startOfDay(),$dateTo->endOfDay()])->get();
        }else{
            $id = Tag::where('id',$id)->firstOrFail();
            $name = 'Statistiky_tagu_'.$id->id;
            $data = $id->tagDailyStats()->whereBetween('date', [$dateFrom->startOfDay(),$dateTo->endOfDay()])->get();
        }
       Excel::create($name.'_od_'.$dateFrom.'_do_'.$dateTo, function($excel) use ($data){
            // Set the title
            $excel->setTitle('Statistiky_');
                //.$type.'_od_'.$dateFrom.'_do_'.$dateTo);

            // Chain the setters
            $excel->setCreator('Centrum bydlení')
                ->setCompany('Centrum bydlení');

            // Call them separately
            //$excel->setDescription('A demonstration to change the file properties');
            //$excel->sheet('list1', function($sheet) {

                $excel->sheet('List1', function($sheet) use($data) {

                    $sheet->fromArray($data);

                });

            })->download('xlsx');


    }

    public function getGlobalExcel(){
        //dd(request());
        $type = request(['type']);
        $dateFrom = $this->dateFrom;
        $dateTo = $this->dateTo;
        //dd($dateFrom);
        if($type=='article'){
            $articles = Article::all();
            $name = 'Statistiky_clanku_';
            foreach($articles as $article){
                $stats = $article->articleDailyStats()->whereBetween('date', [$dateFrom->startOfDay(),$dateTo->endOfDay()])->get();
                foreach($stats as $stat){
                    $data[$stat->id]=array(
                        'id'=>$article->id,
                        'name'=>$article->title,
                        'URL'=>$article->url,
                        'date'=>$stat->date->toDateString(),
                        'clicks_count'=>$stat->clicks_count,
                        'my_count'=>$stat->my_count,
                        'rating_count'=>$stat->rating_count,
                        'avg_rating'=>$stat->avg_rating,

                    );
                }
            }
        }else{
            $tags = Tag::all();
            $name = 'Statistiky_tagu_';
            foreach($tags as $tag){
                $stats = $tag->tagDailyStats()->whereBetween('date', [$dateFrom->startOfDay(),$dateTo->endOfDay()])->get();
                foreach($stats as $stat){
                    //dd($stat);
                    $data[$stat->id]=array(
                        'id'=>$tag->id,
                        'name'=>$tag->name,
                        'URL'=>'/tag/'.$tag->name,
                        'date'=>$stat->date->toDateString(),
                        'clicks_count'=>$stat->clicks_count,
                        'my_count'=>$stat->my_count,
                        'rating_count'=>$stat->rating_count,
                        'avg_rating'=>$stat->avg_rating,
                        'articles_count'=>$stat->articles_count,
                    );
                }
            }
        }
        Excel::create($name.'_od_'.$dateFrom.'_do_'.$dateTo, function($excel) use ($data){
         // Set the title
            $excel->setTitle('Statistiky_');
            //.$type.'_od_'.$dateFrom.'_do_'.$dateTo);

            // Chain the setters
            $excel->setCreator('Centrum bydlení')
                ->setCompany('Centrum bydlení');

            // Call them separately
            //$excel->setDescription('A demonstration to change the file properties');
            //$excel->sheet('list1', function($sheet) {

            $excel->sheet('List1', function($sheet) use($data) {

                $sheet->fromArray($data);

            });

        })->download('xlsx');


    }
}
