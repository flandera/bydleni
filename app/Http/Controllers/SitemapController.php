<?php

namespace App\Http\Controllers;

use App\Http\SitemapCreator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\URL;
use App\Article;
use App\Tag;
use Roumen\Sitemap\Sitemap;
use Illuminate\Contracts\Container\Container;

use App\Http\Requests;

class SitemapController extends Controller
{
    private $siteMap;

    public function __construct()
    {
        //$this->siteMap = $this->makeSitemap();
    }

    public function sitemap(){
        $this->siteMap = new SitemapCreator();
        return "Sitemap created";
    }
}
