<?php

namespace App\Http\Controllers;

use Feeds;
use Illuminate\Http\Request;
use App;
use Illuminate\Support\Facades\Auth;
use willvincent\Feeds\Facades\FeedsFacade;
use willvincent\Feeds\FeedsFactory;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Feed;
use App\Http\Requests\FeedRequest;
use App\Http\Controllers\Auth\AuthController;


class FeedsController extends Controller
{
    public function __construct()
    {

    }
    /** vrací seznam feedu
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    //TODO rozdelit na seznam feedu a servirovani konkretniho feedu
    public function index(){
        $feeds = App\Feed::all();
        return view('feeds.index', compact('feeds'));
    }

    public function create(){
        return view('feeds.create');
    }

    /**
     * save a new article
     * @param FeedRequest $request
     * @return mixed
     */
    private function createFeed(FeedRequest $request)
    {
        $feed = Auth::user()->feeds()->create($request->all());

        return $feed;
    }

    /**
     * @param FeedRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(FeedRequest $request){

        $this->createFeed($request);
        flash()->overlay('Your Feed has been created','good job');
        return redirect('feedy');
    }

    public function edit(Feed $feed){

        return view('feeds.edit',compact('feed'));
    }

    public function show(Feed $feed){
       return view('feeds.show', compact('feed'));
    }

    public function update(Feed $feed, FeedRequest $request){

            $feed->update($request->all());
            //$this->syncTags($article, $request->input('tag_list'));
            return redirect('feedy');
    }
}
