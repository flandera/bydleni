<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $this->getRole();
    }

    public $role;

    public function getRole(){
       if(Auth::guest()){
           $this->role = 'guest';
       }else{
           $this->role = Auth::user()->roles()->firstOrFail()->name;
       }
   }



}
