<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 16.2.2016
 * Time: 11:05
 */

namespace App\Http;

use App\Feed;
use willvincent\Feeds\Facades\FeedsFacade;
use willvincent\Feeds\FeedsFactory;
use App\Article;
use Feeds;
use Illuminate\Database\Eloquent;


class ArticleCrawler
{

    private $items = null;

    public function __construct()
    {

        $this->data = $this->crawl();

    }

    public function crawl(){
        $feeds = Feed::All();
        $items = null;
        $articles = null;

        foreach($feeds as $feed){
            //dd($feed);
            $simplefeed = Feeds::make($feed['url']);
            $simplefeed->strip_htmltags(array_merge($simplefeed->strip_htmltags, array('h1', 'a', 'ul', 'b', 'li', 'br','p','blockquote')));

            $user = 1;
            $data = array(
                'title'     => $simplefeed->get_title(),
                'permalink' => $simplefeed->get_permalink(),
                'items'     => $simplefeed->get_items(),
                'logo'      => $simplefeed->get_image_url(),
            );
            $items = $this->getItems($data);
            //dd($items);
            $articles = $this->saveArticles($items,$feed);
        }



        //dd($data['logo']);

        return;
    }

    public function getItems($data){
        foreach($data['items'] as $item){
            $it['url'] = $item->get_permalink();
            $it['title'] = $item->get_title();
            $it['published_at'] = $item->get_date();
            $it['body'] = preg_replace('/(<.*?>)/', '', html_entity_decode($item->get_content()));
            $it['picture'] = $this->scrapeImage($item->get_content());
            $items[] = $it;
        }
        //dd($items);
        return $items;
    }


    /**
     * @param $items clanky
     * @param Feed $feed clanku
     */
    public function saveArticles($items, Feed $feed){
        $urls = Article::lists('url');
        $articles = array();
        foreach($items as $key => $item){

            if((!($urls->contains($item['url'])))){
                $article = Article::create($item);
                $articles[]=$article->id;

            }

        }
        //dd($articles);
        if(count($articles)!=0){
            $feed->articles()->attach((array)$articles);
        }

        return;
    }

    /**
     * @param $text vstupni text pro ziskani url obrazku
     * @return string link na obrazek
     */
    public function scrapeImage($text) {

       $pattern = '/src=[\'"]?([^\'" >]+)[\'" >]/';
        preg_match($pattern, $text, $link);
        if(!$link==null){
            $link = $link[1];
            $link = urldecode($link);

                $picture = getimagesize ($link);


            if(($picture[0]|$picture[1])<=1){
                $link = null;
            }
        }else{
            $link=null;
        }





        //dd($link);
        return $link;
    }

}