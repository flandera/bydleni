<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 8.2.2016
 * Time: 14:10
 */

namespace App\Http\Requests;


class FeedRequest extends Request
{
    public function authorize()
    {
       return true;
    }

    public function rules(){
        return [
            'title' => 'required|min:3',
        //TODO validace že tam není http
            'url' => array('required','regex://'),
            //'url' => array('required','regex:/(^http|HTTP|Http)\b[a-zA-Z_.]\b/'),
        ];
    }
}