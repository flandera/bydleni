<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 13.5.2016
 * Time: 8:50
 */

namespace App\Http;

use App\ArticleDailyStat;
use App\Article;
use Carbon\Carbon;
use App\Tag;
use App\TagDailyStat;

class ReverseTagStatsCounter
{
    public function __construct()
    {
        $compute = $this->reverseComputeTagsDailyStats();
    }


    public function reverseComputeTagsDailyStats(){
        $tags = Tag::all();
        $stats = [];

        foreach ($tags as $tag) {
            $date = Carbon::parse('2016-05-01');
            $articles = $tag->articles()->whereBetween('article_tag.created_at', [Carbon::parse('2016-05-01'), Carbon::now()->subDay()->endOfDay()])->get();
            //dd(count($articles));
                for ($date; $date <= Carbon::now()->endOfDay()->subDay(1); $date->addDay()) {

                    $tagStat = [];

                    $upperBorder = clone $date;
                    $upperBorder->addDay(1);
                    //echo("<p>".$date."</p>");
                    if (count($articles) != null) {
                        //dd($articles);
                        foreach ($articles as $article) {
                            $hits = $article->clicks()->whereBetween('created_at', [$date->startOfDay(), $upperBorder])->get();
                            $my = $article->users()->whereBetween('article_user.created_at', [$date->startOfDay(),$upperBorder])->get();
                            $rates = $article->ratings()->notNull()->whereBetween('created_at',[$date->startOfDay(),$upperBorder])->get();
                            //dd(['date'=>$date->endOfDay(),'hits'=>$hits,'my'=>$my,'rates'=>$rates]);
                            if (isset($tagStat['id'])) {
                                $tagStat['click_count'] += count($hits);
                                $tagStat['rating_count'] += count($rates);
                                $tagStat['my_count'] += count($my);
                                $tagStat['articles_count']++;
                                $tagStat['avg_rating'] += $article->avg_rating;
                            } else {
                                $tagStat['id'] = $tag->id;
                                $tagStat['click_count'] = count($hits);
                                $tagStat['rating_count'] = count($rates);
                                $tagStat['my_count'] = count($my);
                                $tagStat['avg_rating'] = $article->avg_rating;
                                $tagStat['articles_count'] = 1;
                            }

                        }
                        //dd($tagStat);
                    }
                    if (count($tagStat) != 0) {
                        $tagStat['avg_rating'] = $tagStat['avg_rating'] / $tagStat['articles_count'];
                        if (!($tagStat['click_count'] == 0 && $tagStat['my_count'] == 0 && $tagStat['rating_count'] == 0)) {
                            TagDailyStat::updateOrCreate([
                                'tag_id' => $tag->id,
                                'date' => $date->toDateString()],
                                ['tag_id' => $tag->id,
                                    'date' =>$date->toDateString(),
                                    'clicks_count' => $tagStat['click_count'],
                                    'rating_count' => $tagStat['rating_count'],
                                    'avg_rating' => $tagStat['avg_rating'],
                                    'my_count' => $tagStat['my_count'],
                                    'articles_count' => $tagStat['articles_count']]);
                        }
                    }
//                    echo(['tag_id' => $tag->id,
//                        'date' =>$date->toDateString(),
//                        'clicks_count' => $tagStat['click_count'],
//                        'rating_count' => $tagStat['rating_count'],
//                        'avg_rating' => $tagStat['avg_rating'],
//                        'my_count' => $tagStat['my_count'],
//                        'articles_count' => $tagStat['articles_count']]);
            }

        }

        return;
    }
}