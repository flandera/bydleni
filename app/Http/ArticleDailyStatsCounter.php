<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 2.5.2016
 * Time: 15:03
 */

namespace App\Http;

use App\ArticleWeeklyStatStat;
use App\ArticleMonthlyStat;
use App\ArticleDailyStat;
use App\TagDailyStat;
use App\Article;
use App\Tag;
use Carbon\Carbon;


class ArticleDailyStatsCounter
{
    public function __construct()
    {
        //$compute = $this->reverseCompute();
        $computeArticles = $this->computeDailyStats();
        $computeTags = $this->computeTagsDailyStats();

    }

    public function computeDailyStats(){
        $articles = Article::all();
        foreach ($articles as $article) {
            $stat = $this->getArticleStats($article);
            if(!(count($stat['hits'])==0&&count($stat['my'])==0&&count($stat['rates'])==0)) {
                    ArticleDailyStat::updateOrCreate([
                    'article_id'=>$article->id,
                    'date'=>Carbon::now()->toDateString()],
                    ['article_id'=>$article->id,
                        'date'=>Carbon::now()->toDateString(),
                        'clicks_count'=>count($stat['hits']),
                        'rating_count'=>count($stat['rates']),
                        'avg_rating'=>$article->avg_rating,
                        'my_count'=>count($stat['my'])]);
            }
        }
        return;
    }

    public function computeTagsDailyStats(){
        $tags = Tag::all();
        $stats = [];
        foreach ($tags as $tag){
            $articles = $tag->articles()->get();
            $tagStat = [];
            if(count($articles)!=null){
                foreach ($articles as $article) {
                    $stat = $this->getArticleStats($article);
                    //dd($stat);
                    if(isset($tagStat['id'])){
                            $tagStat['click_count']+=count($stat['hits']);
                            $tagStat['rating_count']+=count($stat['rates']);
                            $tagStat['my_count']+=count($stat['my']);
                            $tagStat['articles_count']++;
                            $tagStat['avg_rating']+=$article->avg_rating;
                    }else{
                            $tagStat['id']=$tag->id;
                            $tagStat['name']=$tag->name;
                            $tagStat['url']='/tag/'.$tag->name;
                            $tagStat['click_count']=count($stat['hits']);
                            $tagStat['rating_count']=count($stat['rates']);
                            $tagStat['my_count']=count($stat['my']);
                            $tagStat['avg_rating']=$article->avg_rating;
                            $tagStat['articles_count']=1;
                        }

                    }
                }
            if(count($tagStat)!=0){
                $tagStat['avg_rating'] = $tagStat['avg_rating']/ $tagStat['articles_count'];
                if(!($tagStat['click_count']==0&&$tagStat['my_count']==0&& $tagStat['rating_count']==0)) {
                    TagDailyStat::updateOrCreate([
                        'tag_id'=>$tag->id,
                        'date'=>Carbon::now()->toDateString()],
                        ['tag_id'=>$tag->id,
                            'date'=>Carbon::now()->toDateString(),
                            'clicks_count'=>$tagStat['click_count'],
                            'rating_count'=>$tagStat['rating_count'],
                            'avg_rating'=>$tagStat['avg_rating'],
                            'my_count'=> $tagStat['my_count'],
                            'articles_count'=> $tagStat['articles_count']]);
                }
            }

            //dd($tagStat);

            }


        return;
    }


    public function getArticleStats(Article $article){
            $previousDay = Carbon::now()->endOfDay()->subDay(1);
//dd($previousDay);
            $hits = $article->clicks()->whereBetween('created_at', [$previousDay, Carbon::now()->endOfDay()])->get();
            $my = $article->users()->whereBetween('article_user.created_at', [$previousDay, Carbon::now()->endOfDay()])->get();
            $rates = $article->ratings()->whereBetween('created_at', [$previousDay, Carbon::now()->endOfDay()])->get();
        //dd($my);
        return ['hits'=>$hits, 'my'=>$my, 'rates'=>$rates];
    }

}