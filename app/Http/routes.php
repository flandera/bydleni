<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/








/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/



Route::group(['middleware' => 'web'], function () {

    Route::auth();
    Route::get('sitemap.xml', function(){

    });
    Route::get('/clanky/hledat','ArticlesController@search');
    Route::get('/clanky/unauthorized','ArticlesController@unauthorized');

    Route::get('/clanky/moje','ArticlesController@my');
    Route::get('/sitemap','SitemapController@sitemap');


    Route::resource('tag', 'TagsController');




    Route::group(['middleware' => 'auth'], function () {
        Route::group(['middleware' => 'admin'], function () {
            Route::resource('feedy', 'FeedsController'
//            [
//            'names' => [
//                'create' => 'novy',
//                'show' => 'detail',
//                ]
            );
            Route::get('statistiky/compute', 'StatsController@compute');
            Route::get('statistiky/computetags', 'StatsController@computeTags');
           // Route::get('statistiky/download', [ 'as' => 'statistiky.download', 'uses' => 'StatsController@getExcel',]);
            Route::get('/statistiky/download/{id}', [ 'as' => 'statistiky.download', 'uses' => 'StatsController@getExcel',]);
            Route::get('/statistiky/download', [ 'as' => 'statistiky.download', 'uses' => 'StatsController@getGlobalExcel',]);
            Route::get('/statistiky/tag/{id}', [ 'as' => 'statistiky.showTag', 'uses' => 'StatsController@showTag',]);
//            Route::get('statistiky/tags/{tag}', 'StatsController@showTag');
            Route::get('statistiky/tags', 'StatsController@tags');


            Route::resource('statistiky', 'StatsController');


        });
    });
    Route::post('/clanky/{clanky}/addclick', 'ArticlesController@addClick');
    Route::post('/clanky/{clanky}/addMyArticle', 'ArticlesController@addMyArticle');
    Route::post('/clanky/{clanky}/removeMyArticle', 'ArticlesController@removeMyArticle');
    Route::post('/clanky/{clanky}/rate', 'ArticlesController@rate');
    Route::resource('clanky', 'ArticlesController');
    Route::get('/home', function(){
        return redirect('/',301);
    });

    Route::get('/', 'ArticlesController@index');


});
