<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleWeeklyStat extends Model
{
    protected $fillable = [
        'article_id',
        'clicks_count',
        'my_count'
    ] ;

    public function article(){
        return $this->belongsTo('App\Article');
    }
}
