<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TagDailyStat extends Model
{
    protected $dates = ['date'];

    protected $fillable = [
        'tag_id',
        'date',
        'clicks_count',
        'my_count',
        'rating_count',
        'avg_rating',
        'articles_count'
    ] ;
    public function tag(){
        return $this->belongsTo('App\Tag');
    }
}
