<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Rating extends Model
{
    protected $fillable = [
        'value',
        'article_id',
        'user_id'
    ];
    //protected $primaryKey = ['article_id','user_id'];
    /**
     *prirazeni ratu k uzivateli
     */
    public function user(){
        $this->belongsTo('App\User');
    }
    /**
     *prirazeni ratu k clanku
     */
    public function article(){
        $this->belongsTo('App\Article');
    }

    //nepouziva se
    public function scopeUsersRating($query){
        $query->where('user_id',Auth::user()->id);
    }

    /**
     * @param $query
     * Scope pro vytazeni id administratoru a daneho uzivatele
     */
    public function scopeAdminRating($query){
        $admins = $this->getAdmins();
        if(!Auth::guest()){
            $admins[] = Auth::user()->id;
        }

        $query->whereIn('user_id',$admins);
    }

    /**
     * @return mixed list id administratoru
     */
    public function getAdmins(){
        $role =  Role::where('name','admin')->firstOrFail();
        return $role->users()->lists('user_id');
    }

    public function scopeNotNull($query){
        $query->whereNotNull('value');
    }
}
