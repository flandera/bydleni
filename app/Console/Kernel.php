<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\CrawleArticles::class,
        Commands\StatsCount::class,
        Commands\StoreSitemap::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('articles:crawl')
                  ->hourly();
//        $schedule->command('stats:count')
//            ->dailyAt('00:00');
        $schedule->command('stats:count')
            ->hourly();
        $schedule->command('stats:count')
            ->dailyAt('23:59');
        $schedule->command('store:sitemap')
            ->hourly();
    }
}
