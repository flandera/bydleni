<?php

namespace App\Console\Commands;

use App\Http\ArticleCrawler;
use Illuminate\Console\Command;

class CrawleArticles extends Command
{
    private $crawler = null;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'articles:crawl';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->crawler = new ArticleCrawler();
    }
}
