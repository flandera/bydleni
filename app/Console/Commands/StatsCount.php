<?php

namespace App\Console\Commands;

use App\Http\ArticleDailyStatsCounter;
use Illuminate\Console\Command;


class StatsCount extends Command
{
    private $statsCounter = null;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stats:count';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Counts daily stats';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->statsCounter = new ArticleDailyStatsCounter();
    }
}
