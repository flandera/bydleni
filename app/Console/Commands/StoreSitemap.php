<?php

namespace App\Console\Commands;

use App\Http\SitemapCreator;
use Illuminate\Console\Command;

class StoreSitemap extends Command
{
    private $siteMapCreator = null;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'store:sitemap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates new sitemap';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $this->siteMapCreator = new SitemapCreator();
    }
}
