<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function scopeAdmins($query){
        $query->where('user_id',$this->getAdmins());
    }

    public function feeds(){
        return $this->hasMany('App\Feed');
    }


//    public function articles(){
//        return $this->belongsToMany('App\Article', 'article_tag', 'user_id','article_id')->withPivot('tag_id')->withTimestamps();
//        //;
//    }

    public function ratings(){
        return $this->hasMany('App\Rating');
    }

    public function roles(){
        return $this->belongsToMany('App\Role');
    }

//    public function tags(){
//        return $this->belongsToMany('App\Tag', 'article_tag', 'user_id', 'tag_id')->withPivot('article_id')->withTimestamps();
//        //->withPivot('article_id');
//    }

    public function getAdmins(){
        //dd(Role::users()->where('name','admin')->lists('user_id'));
        return Role::users()->where('name','admin')->lists('user_id');
    }

    public function assignRole($role){
        return $this->roles()->save(
          Role::whereName($role)->firstOrFail()
        );
    }

    public function hasRole($role){
        if(is_string($role)){
            return $this->roles->contains('name',$role);
        }

        foreach($role as $r){
            if($this->hasRole($r->name)){
                return true;
            }
        }
        return false;
    }

    public function articles(){
        return $this->belongsToMany('App\Article')->withTimestamps();
    }

}
