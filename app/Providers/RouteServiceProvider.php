<?php

namespace App\Providers;

use App\Feed;
use App\Article;
use App\Rating;
use App\Tag;
use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        $router->bind('feedy',function($id){

            return Feed::where('id',$id)->firstOrFail();
        });

        $router->bind('clanky',function($id){

            return Article::where('id',$id)->firstOrFail();
        });

        $router->bind('statistiky',function($id){
            return Article::where('id',$id)->firstOrFail();
        });


        $router->bind('tag',function($name){
            return Tag::where('name', $name)->firstOrFail();
        });

        $router->bind('ratings',function($article){
            return Rating::where('article_id', $article)->firstOrFail();
        });
        $router->bind('clicks',function($article){
            return Rating::where('article_id', $article)->firstOrFail();
        });
        $router->bind('statistiky/tag',function($id){
           return Tag::where('id', $id)->firstOrFail();
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
