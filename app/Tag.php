<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = [
        'name',

    ];
    /**
     * return articles associated with given tag
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function articles(){
        return $this->belongsToMany('App\Article');
        //->withPivot('user_id');
    }

    public function tagDailyStats(){
        return $this->hasMany('App\TagDailyStat');
    }

//    public function users(){
//        return $this->belongsToMany('App\User', 'article_tag', 'tag_id', 'user_id')->withPivot('article_id');
//    }

//    public function setUserIdAttribute(){
//        $this->attributes['user_id'] = Auth::user()->id;
//    }
//    public function getUserIdAttribute(){
//        return Auth::user()->id;
//    }
}
