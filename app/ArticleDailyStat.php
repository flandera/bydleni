<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleDailyStat extends Model
{
    protected $dates = ['date'];

    protected $fillable = [
       'article_id',
        'date',
        'clicks_count',
        'my_count',
        'rating_count',
        'avg_rating'
    ] ;
    public function article(){
        return $this->belongsTo('App\Article');
    }
}
