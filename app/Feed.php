<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Feed extends Model
{
    protected $fillable = [
        'title',
        'url',
        'user_id'
    ];


    /**
     *prirazeni feedu k uzivateli
     */
    public function user(){
        $this->belongsTo('App\User');
    }

    public function articles(){
        return $this->belongsToMany('App\Article')->withTimestamps();
    }
}
