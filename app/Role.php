<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use App\Permission;

class Role extends Model
{
    protected $fillable = [
            'name',
            'label',
    ];

    public function users(){
        return $this->belongsToMany('App\User');
    }

    public function permissions(){
        return $this->belongsToMany('App\Permission');
    }

    public function givePermissionTo(Permission $permission){

        return $this->permissions()->save($permission);
    }
}
