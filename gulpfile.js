var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
elixir.config.assetsDir = 'public/';

elixir(function(mix) {
    mix.less('app.less');

    mix.styles([
        'bootstrap.css',
        'starter-template.css',
        'select2.css',
        'star-rating.css',
        'theme-krajee-svg.css',
        'jRating.jquery.css',
        'jquery-ui.css',
        'app.css',


    ], 'public/css/all.css', 'public/css');

    mix.version('public/css/all.css');

    mix.scripts([
        'jquery.js',
        'bootstrap.js',
        'select2.js',
        'star-rating.js',
        'jRating.jquery.js',
        'jquery.session.js',
        'jquery-ui.js',
        'Chart.js'


    ],'public/js/scripts.js', 'public/js');

    //mix.phpUnit();

    //mix.version('public/js/scripts.js');
});
