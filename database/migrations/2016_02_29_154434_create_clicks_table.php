<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClicksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clicks', function (Blueprint $table){
        $table->increments('id');
        $table->integer('article_id')->unsigned();
        $table->foreign('article_id')->references('id')->on('articles')->onDelete('cascade');
        $table->integer('user_id')->unsigned()->nullable();
        $table->text('ip');
        $table->nullableTimestamps();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clicks');
    }
}
