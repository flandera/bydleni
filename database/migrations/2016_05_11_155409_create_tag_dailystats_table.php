<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagDailystatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tag_daily_stats', function (Blueprint $table){
            $table->increments('id');
            $table->date('date');
            $table->integer('tag_id')->unsigned();
            $table->foreign('tag_id')->references('id')->on('tags')->onDelete('cascade');
            $table->integer('clicks_count')->unsigned()->nullable();
            $table->integer('my_count')->unsigned()->nullable();
            $table->integer('rating_count')->unsigned()->nullable();
            $table->integer('articles_count')->unsigned()->nullable();
            $table->float('avg_rating')->nullable();
            $table->nullableTimestamps();
            $table->unique(['tag_id','date']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('tag_daily_stats');
    }
}
