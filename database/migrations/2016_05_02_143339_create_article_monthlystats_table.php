<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleMonthlystatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_monthly_stats', function (Blueprint $table){
            $table->increments('id');
            $table->date('date');
            $table->integer('article_id')->unsigned();
            $table->foreign('article_id')->references('id')->on('articles')->onDelete('cascade');
            $table->integer('clicks_count')->unsigned()->nullable();
            $table->integer('my_count')->unsigned()->nullable();
            $table->integer('rating_count')->unsigned()->nullable();
            $table->float('avg_rating')->nullable();
            $table->nullableTimestamps();
            $table->unique(['article_id','date']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('article_monthly_stats');
    }
}
