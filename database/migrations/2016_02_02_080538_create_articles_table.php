<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            //$table->integer('user_id')->unsigned();
            $table->string('title');
            $table->text('body');
            $table->text('url');
            $table->text('picture')->nullable();
            $table->float('avg_rating');
            $table->nullableTimestamps();
            $table->timestamp('published_at');
        });

        Schema::create('article_feed', function (Blueprint $table){
            $table->increments('id');
            $table->integer('feed_id')->unsigned();
            $table->foreign('feed_id')->references('id')->on('feeds')->onDelete('cascade');
            $table->integer('article_id')->unsigned();
            $table->foreign('article_id')->references('id')->on('articles')->onDelete('cascade');
            $table->nullableTimestamps();
            $table->unique(['feed_id','article_id']);
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::Drop('article_feed');
        Schema::Drop('articles');
    }
}
