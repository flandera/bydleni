<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles[] = new App\Role(['name'=>'admin','label'=>'administrator']);
        $roles[] = new App\Role(['name'=>'user','label'=>'uživatel']);
        foreach($roles as $role){
            $role->save();
        }
    }
}
