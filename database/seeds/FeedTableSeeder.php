<?php

use Illuminate\Database\Seeder;

class FeedTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $feeds[] = new App\Feed(['title'=>'nový','url'=>'http://www.novinky.cz/rss2/bydleni/','user_id'=>1]);
        $feeds[] = new App\Feed(['title'=>'lépe bydlet','url'=>'http://www.lepebydlet.cz/feed','user_id'=>1]);
        foreach($feeds as $feed){
            $feed->save();
        }

    }
}
