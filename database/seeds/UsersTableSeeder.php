<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new App\User(['name'=>'unregistered','email'=>'nobody@nothing.cz','password'=>'$2y$10$LQ3F9zWQj2xs/PbhEGR7SunUpPMBEQLaOH6ka2nI1QFNtIkvJ59/6']);
        $user->save();
        $user = new App\User(['name'=>'martin','email'=>'mdrake@volny.cz','password'=>'$2y$10$lyYltd1/xVzHQ7YjDCycZO6qH1tgtsrlavF52/1Sw1iVIHc0AsSva']);
        $user->save();
        $user->roles()->sync([2]);
        $user = new App\User(['name'=>'admin','email'=>'admin@admin.cz','password'=>'$2y$10$svURBhbYnR7Ex11jIs32VunXTmTFXEuAGGgaWcJBzNY.qXQm73IyS']);
        $user->save();
        $user->roles()->sync([1]);

    }
}
